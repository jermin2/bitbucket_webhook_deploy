# bitbucket_webhook_deploy

## Requirements
SSH without password setup. (Maybe, I'm not sure). But you need to setup the IdentityFile in the ssh config as shown here:
http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/
and

Edit your **~/.ssh/config** file to add _bitbucket.org_ as a host. This ensures that the correct key is used when connecting by SSH to target host. 
You'll need to create the config file if it doesn't exist:

```
Host github.com
    IdentityFile ~/.ssh/<your_private_key_file>
```

Make sure you use your PRIVATE key, not your PUBLIC key (HINT: it doesn't end with .pub)

Then you just have to set the webhook setting in BitBucket to trigger on a push request

## Setup
Edit the bitbucket-hook.php file variables

### Repo locations
```
$testrepo_dir = location of your test setup
$repo_dir = location of your deployment setup
$web_root_dir = public_html folder or www folder
```

### Branch Names
```
$test_branch = "release/test"
$deploy_branch = "release/deployment"
```

That's it. Try committing a change and see if it works

## See also

- [Original Jonathan Nicoal's page](http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/)
- [More updated version of above page](https://bitbucket.org/lilliputten/automatic-bitbucket-deploy/src/master/)
