<?php
//REPO LOCATIONS
$testrepo_dir = '/home/$USER/$TEST_REPO_LOC';
$repo_dir = '/home/$USER/$REPO_LOC';
$web_root_dir = '/home/$USER/public_html';

//BRANCH NAMES
$test_branch = "release/test";
$deploy_branch = "release/deployment";

// Full path to git binary is required if git is not in your PHP user's path. Otherwise just use 'git'.
$git_bin_path = 'git';

$update_test = false;
$update_deploy = false;

// Parse data from Bitbucket hook payload
//$PAYLOAD = json_decode($_POST['payload']);
if ( isset($_POST['payload']) ) { // old method
    $PAYLOAD = $_POST['payload'];
} else { // new method
    $PAYLOAD = json_decode(file_get_contents('php://input'));
}

//If there are changes
if (isset($PAYLOAD->push->changes)) {
    foreach ( $PAYLOAD->push->changes as $change ) {
        // TODO: Fetch branch name for github from `$PAYLOAD->ref`
        if (is_object($change->new) && $change->new->type == "branch") {
            $branchName = $change->new->name;
            if($branchName == $test_branch){
                $update_test = true;
            }
            if($branchName == $deploy_branch){
                $update_deploy = true;
            }
        }
    }
}

//UPDATE THE TEST REPO
//====================================================
if ($update_test) {
    
  // Do a git checkout to the web root
  $fetch_hash = shell_exec('cd ' . $testrepo_dir . ' && ' . $git_bin_path  . ' fetch -v');
  $checkout_hash = shell_exec('cd ' . $testrepo_dir . ' && ' . $git_bin_path  . ' reset --hard origin/release/test');
  
    // Log the deployment
  $commit_hash = shell_exec('cd ' . $testrepo_dir . ' && ' . $git_bin_path  . ' rev-parse --short HEAD');
  file_put_contents('deploy.log', date('m/d/Y h:i:s a') . " Deployed branch: " .  $test_branch . " Commit: " . $commit_hash . " Fetch: " . $fetch_hash . " checkout: " . $checkout_hash . "\n", FILE_APPEND);
  
}

//UPDATE THE DEPLOYMENT REPO
//====================================================
if($update_deploy){
  
    // Do a git checkout to the web root
  $fetch_hash = shell_exec('cd ' . $repo_dir . ' && ' . $git_bin_path  . ' fetch -v');
  $checkout_hash = shell_exec('cd ' . $repo_dir . ' && ' . $git_bin_path  . ' reset --hard origin/release/deployment');
  
  // Log the deployment
  $commit_hash = shell_exec('cd ' . $repo_dir . ' && ' . $git_bin_path  . ' rev-parse --short HEAD');
  file_put_contents('deploy.log', date('m/d/Y h:i:s a') . " Deployed branch: " .  $deploy_branch . " Commit: " . $commit_hash . " Fetch: " . $fetch_hash . " checkout: " . $checkout_hash . "\n", FILE_APPEND);
  
}
?>